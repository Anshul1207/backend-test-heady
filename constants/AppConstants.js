module.exports = {
    ERROR_MESSAGES: {
        ALREADY_EXISTS: 'category already exists',
        PRODUCT_ALREADY_EXISTS: 'product already exists to add to a category use route /addProductToCategory',
        ALL_FIELDS_REQUIRED: 'all fields are required',
        VALIDATION_ERROR: 'validation error',
        INTERNAL_SERVER_ERROR: 'internal server error',
    }
}