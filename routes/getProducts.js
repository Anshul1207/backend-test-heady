const product = require('../modules/getProducts.js');
module.exports = function(app){
    app.get('/getProducts:categoryId', product.getProducts);
};