const product = require('../modules/addProductToCategory.js');
module.exports = function(app){
    app.post('/addProductToCategory', product.addProductToCategory);
};