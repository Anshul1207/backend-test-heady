module.exports = (app) => {
    app.get('/', (req, res) => {
        res.json({works: true});
    });
    // require("./register")(app);
    // require("./login")(app);
    // require("./post")(app);
    require("./category")(app);
    require("./product")(app);
    require("./updateProduct")(app);
    require("./getCategories")(app);
    require("./addProductToCategory")(app);
    require("./getProducts")(app);
};