const product = require('../modules/updateProduct.js');
module.exports = function(app){
    app.post('/updateProduct', product.updateProduct);
};