const category = require('../modules/getCategories.js');
module.exports = function(app){
    app.get('/getCategories', category.getCategories);
};
