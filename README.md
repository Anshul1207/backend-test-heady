# steps to run the application. #

### steps to run using nodemon ###
* npm install
* npm install -g nodemon
* nodemon server.js

### steps to run using docker ###
* docker build .
* docker-compose up

### API postman collection ###
* backend-test.postman_collection.json

if any doubts feel free to mail or call me @ anshul.dharmadhikari@live.com | +91-8884567675