// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants');
// models
const categoryModel = require('../models/Category'),
    productModel = require('../models/Product');

exports.addProduct = async function(req, res) {
    const extra = {
        res: res
    };
    if (req.body && req.body.name && req.body.price && req.body.category_ids) {
        const { name, price, category_ids } = req.body;
        // save product
        const [insertError, insertResponse] = await pHandler(productModel.create(
            { 
                name,
                price,
                category_ids
            }
        ));
        if (insertError && insertError.code === 11000) {
            utilityManager.throwError(appConstants.ERROR_MESSAGES.PRODUCT_ALREADY_EXISTS, appConstants.ERROR_MESSAGES.PRODUCT_ALREADY_EXISTS, res, headerConstants.FAIL.SERVER_ERROR);
        }
        // update category
        category_ids.forEach(async id => {
            const [ updateError, updateResponse ] = await pHandler(categoryModel.update(
                {
                    _id: id
                },
                {
                    $push : { products : { $each : [insertResponse._id] } } 
                }
            ));
        });
        console.log('insertResponse', insertResponse);
        responseManager(null, insertResponse, extra);

    } else {
        utilityManager.throwError(appConstants.ERROR_MESSAGES.ALL_FIELDS_REQUIRED, appConstants.ERROR_MESSAGES.VALIDATION_ERROR, res, headerConstants.FAIL.BAD_REQUEST);
    }
};
