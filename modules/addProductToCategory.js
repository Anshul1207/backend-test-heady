// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants');
// models
const categoryModel = require('../models/Category'),
    productModel = require('../models/Product');

exports.addProductToCategory = async function(req, res) {
    const extra = {
        res: res
    };
    if (req.body && req.body.productId && req.body.categoryIds) {
        const { productId, categoryIds } = req.body;
        // update category
        categoryIds.forEach(async id => {
            const [ updateCategoryError, updateCategoryResponse ] = await pHandler(categoryModel.updateOne(
                {
                    _id: id
                },
                {
                    $push : { products : { $each : [productId] } } 
                }
            ));
            const [ updateError, updateResponse ] = await pHandler(productModel.updateOne(
                {
                    _id: productId
                },
                {
                    $push : { category_ids : { $each : [id] } } 
                }
            ));
            console.log('updateResponse', updateResponse);
            responseManager(null, updateResponse, extra);
        });

    } else {
        utilityManager.throwError(appConstants.ERROR_MESSAGES.ALL_FIELDS_REQUIRED, appConstants.ERROR_MESSAGES.VALIDATION_ERROR, res, headerConstants.FAIL.BAD_REQUEST);
    }
};
