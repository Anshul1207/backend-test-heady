// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants');
// models
const categoryModel = require('../models/Category'),
    productModel = require('../models/Product');

exports.addCategory = async function(req, res) {
    const extra = {
        res: res
    };
    if (req.body && req.body.category) {
        if (req.body.productId) {
            const [insertError, insertResponse] = await pHandler(categoryModel.create(
                {
                    category: req.body.category,
                    productId: req.body.productId
                }
            ));
            if (insertError && insertError.code === 11000) {
                utilityManager.throwError(appConstants.ERROR_MESSAGES.ALREADY_EXISTS, appConstants.ERROR_MESSAGES.ALREADY_EXISTS, res, headerConstants.FAIL.SERVER_ERROR);
            }
            const [ updateError, updateResponse ] = await pHandler(productModel.updateOne(
                {
                    _id: mongoose.Types.ObjectId(req.body.productId)
                },
                {
                    $push : { categories : { $each : [insertResponse._id] } } 
                }
            ));
            console.log('insertResponse', insertResponse);
            responseManager(null, insertResponse, extra);
        } else if (req.body.parentCategory) {
            console.log('inside parent category', req.body);
            const [insertError, insertResponse] = await pHandler(categoryModel.create(
                {
                    category: req.body.category,
                    parentCategory:req.body.parentCategory
                }
            ));
            console.log('err parent', insertError);
            if (insertError && insertError.code === 11000) {
                utilityManager.throwError(appConstants.ERROR_MESSAGES.ALREADY_EXISTS, appConstants.ERROR_MESSAGES.ALREADY_EXISTS, res, headerConstants.FAIL.SERVER_ERROR);
            }
            const [ updateError, updateResponse ] = await pHandler(categoryModel.updateOne(
                {
                    _id: mongoose.Types.ObjectId(req.body.parentCategory)
                },
                {
                    $push : { child_categories : { $each : [insertResponse._id] } } 
                }
            ));
            console.log('insertResponse', insertResponse);
            responseManager(null, insertResponse, extra);
        } else {
             // save category
            const [insertError, insertResponse] = await pHandler(categoryModel.create(
                {
                    category: req.body.category
                }
            ));
            console.log('err', insertError);
            if (insertError && insertError.code === 11000) {
                utilityManager.throwError(appConstants.ERROR_MESSAGES.ALREADY_EXISTS, appConstants.ERROR_MESSAGES.ALREADY_EXISTS, res, headerConstants.FAIL.SERVER_ERROR);
            }
            console.log('insertResponse', insertResponse);
            responseManager(null, insertResponse, extra);
        }


        // const [findOneError, findOneResponse] = await pHandler(userModel.findOne(
        //     {
        //         loginToken: req.headers.token
        //     }
        // ));
        // if (findOneError) {
        //     console.log('findOneError', typeof findOneError.code);
        //     utilityManager.throwError(appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, appConstants.ERROR_MESSAGES.NOT_FOUND, res, headerConstants.FAIL.SERVER_ERROR);
        // }
        // console.log('findOneResponse', findOneResponse);
        // if (findOneResponse === null) {
        //     utilityManager.throwError(appConstants.ERROR_MESSAGES.VALIDATION_ERROR, appConstants.ERROR_MESSAGES.UNAUTHORIZED_ACCESS, res, headerConstants.FAIL.UNAUTHORIZED);
        // } else {
        //     const { comment, postId } = req.body;
        //     if (req.body.commentId) {
        //         // save new comment
        //         const [insertError, insertResponse] = await pHandler(commentModel.create(
        //             {
        //                 comment,
        //                 postId,
        //                 user: {
        //                     _id: findOneResponse._id,
        //                     name: findOneResponse.name
        //                 },
        //                 commentId: req.body.commentId
        //             }
        //         ));
        //         if (insertError) {
        //             console.log('insertError', insertError);
        //             utilityManager.throwError(appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, res, headerConstants.FAIL.SERVER_ERROR);
        //         }
        //         console.log('insertResponse', insertResponse);
        //         responseManager(null, insertResponse, extra);
        //         // update parentComment with childComment ID
        //         const [ updateError, updateResponse ] = await pHandler(commentModel.update(
        //             {
        //                 _id: req.body.commentId
        //             },
        //             {
        //                 $push : { comments : { $each : [insertResponse._id] } } 
        //             }
        //         ));
        //         if (updateError) {
        //             console.log('updateError', updateError);
        //             utilityManager.throwError(appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, appConstants.ERROR_MESSAGES.NOT_FOUND, res, headerConstants.FAIL.SERVER_ERROR);
        //         }
        //         console.log('updateResponse', updateResponse);
        //     } else {
        //         // save comment
        //         const [insertError, insertResponse] = await pHandler(commentModel.create(
        //             {
        //                 comment,
        //                 postId,
        //                 user: {
        //                     _id: findOneResponse._id,
        //                     name: findOneResponse.name
        //                 },
        //                 parent: true
        //             }
        //         ));
        //         if (insertError) {
        //             console.log('insertError', typeof insertError.code);
        //             utilityManager.throwError(appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, res, headerConstants.FAIL.SERVER_ERROR);
        //         }
        //         console.log('insertResponse', insertResponse);
        //         responseManager(null, insertResponse, extra);
        //         // update post document
        //         const [ updateError, updateResponse ] = await pHandler(postModel.update(
        //             {
        //                 _id: postId
        //             },
        //             {
        //                 $push : { comments : { $each : [insertResponse._id] } }
        //             }
        //         ));
        //         if (updateError) {
        //             console.log('updateError', updateError);
        //             utilityManager.throwError(appConstants.ERROR_MESSAGES.INTERNAL_SERVER_ERROR, appConstants.ERROR_MESSAGES.NOT_FOUND, res, headerConstants.FAIL.SERVER_ERROR);
        //         }
        //         console.log('updateResponse', updateResponse);
        //     }
        // }
    } else {
        utilityManager.throwError(appConstants.ERROR_MESSAGES.ALL_FIELDS_REQUIRED, appConstants.ERROR_MESSAGES.VALIDATION_ERROR, res, headerConstants.FAIL.BAD_REQUEST);
    }
};
