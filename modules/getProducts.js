// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants'),
    ObjectId = require('mongodb').ObjectID;
// models
const productModel = require('../models/Product');

exports.getProducts = async function(req, res) {
    const extra = {
        res: res
    };
    console.log('object id', mongoose.Types.ObjectId(req.query.categoryId));
    const [findError, findResponse] = await pHandler(productModel.find({
        category_ids : mongoose.Types.ObjectId(req.query.categoryId)
    }).sort({ createdAt: -1 }));
    console.log('findOneResponse', findResponse);
    responseManager(null, findResponse, extra);
};
