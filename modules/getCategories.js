// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants');
// models
const categoryModel = require('../models/Category');

exports.getCategories = async function(req, res) {
    const extra = {
        res: res
    };
    const [findError, findResponse] = await pHandler(categoryModel.find().populate({
        path: 'child_categories',
        options: { sort: { 'createdAt': -1 } }
    }).sort({ createdAt: -1 }));
    console.log('findOneResponse', findResponse);
    responseManager(null, findResponse, extra);
};
