// helper files
const pHandler = require('../helpers/PromiseHandler'),
    responseManager = require('../helpers/ResponseManager'),
    headerConstants = require('../constants/HeaderConstants'),
    utilityManager = require('../helpers/Utility'),
    appConstants = require('../constants/AppConstants');
// models
const productModel = require('../models/Product');

exports.updateProduct = async function(req, res) {
    const extra = {
        res: res
    };
    if (req.body.id && req.body.data) {
        const { id, data } = req.body;
        // update category
        const [ updateError, updateResponse ] = await pHandler(productModel.update(
            {
                _id: id
            },
            {
                $set : data
            }
        ));
            
        console.log('updateResponse', updateResponse);
        responseManager(null, updateResponse, extra);

    } else {
        utilityManager.throwError(appConstants.ERROR_MESSAGES.ALL_FIELDS_REQUIRED, appConstants.ERROR_MESSAGES.VALIDATION_ERROR, res, headerConstants.FAIL.BAD_REQUEST);
    }
};
