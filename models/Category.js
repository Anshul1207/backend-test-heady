const Schema = mongoose.Schema;

const CategorySchema = new Schema(
    {

        category: { type: String, required: true, unique: true },
        parentCategory: { type: Schema.Types.ObjectId },
        productId: { type: Schema.Types.ObjectId },
        child_categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
        products: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
        createdAt: { type: Date, default: Date.now }
    },
    {
        collection: 'categories'
    }
);
CategorySchema.pre('find', function(next) {
    this.populate({path: 'child_categories', options: { sort: { 'createdAt': -1 } } });
    next();
  });
module.exports = mongoose.model('Category', CategorySchema);