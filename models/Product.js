const Schema = mongoose.Schema;
const ProductSchema = new Schema(
    {

        name: { type: String, required: true, unique: true },
        price: { type: String, required: true },
        category_ids: [{ type: Schema.Types.ObjectId, required: true }],
        categories: [{ type: Schema.Types.ObjectId, ref: 'Category' }],
        createdAt: { type: Date, default: Date.now }
    },
    {
        collection: 'products'
    }
);

module.exports = mongoose.model('Product', ProductSchema);